import pandas
from sklearn.model_selection import train_test_split

from sklearn.ensemble import RandomForestClassifier

from suppport_function import Correlation_clean
from suppport_function import Multivariate_analysis
from suppport_function import Multivariate_analysis_MCA
from suppport_function import print_stats

from sklearn.svm import SVC
from imblearn.over_sampling import SMOTE

# Setting seed for result reproducebility
seed = 10


data = pandas.read_csv('risk_group.txt',sep='\t')

for i in range(len(data)):
    # risk == 2 means FHR else 0
    if data.at[i,'risk_com'] < 2:
        data.at[i,'risk_com'] = 0
    elif data.at[i,'risk_com'] == 2:
        data.at[i,'risk_com'] = 1

feature_matrix = data[list(data.columns)[:-1]]
target = data[list(data.columns)[-1]]

# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(feature_matrix, target, test_size=0.3,random_state=seed) # 70% training and 30% test

X_train_list = list(X_train['Patient'].values)
X_test_list = list(X_test['Patient'].values)

print(X_train_list)

##################################
# Reading cna_by_arm Data
##################################
print('##################################')
print('# Reading cna_by_arm Data')
print('##################################')

data_cna_by_arm = pandas.read_csv('cna_by_arm+Pid.csv',sep='\t')
interm = data_cna_by_arm[list(data_cna_by_arm.columns)[2:]]

interm = Correlation_clean(interm,seed=seed,correlation_threshold=0.5,filename='corr_all_cna_by_arm.png')
interm = Multivariate_analysis(interm,seed=seed)
interm_list = list(interm.columns)
interm_list.append('Patient')
data_cna_by_arm = data_cna_by_arm[interm_list]
Correlation_clean(data_cna_by_arm,seed=seed,correlation_threshold=0.5,filename='corr_selected_cna_by_arm.png')
data_cna_by_arm = data_cna_by_arm.set_index('Patient')

Train = data_cna_by_arm.loc[X_train_list,:].reset_index()
Train = Train[list(Train.columns)[1:]]
Test = data_cna_by_arm.loc[X_test_list,:].reset_index()
Test = Test[list(Test.columns)[1:]]

print('---------------------------------------------------')
print('---------------------------------------------------')
print('interm list')
feature_names = pandas.DataFrame(columns=list(Train.columns)[:-1], dtype='object')
feature_names.to_csv('features_cna_by_arm+Pid.csv',sep='\t',index=False)
print(list(Train.columns))

print('---------------------------------------------------')
print('---------------------------------------------------')



X_train = Train[list(Train.columns)[:-1]]
y_train = Train[list(Train.columns)[-1]]

X_test = Test[list(Test.columns)[:-1]]
y_test = Test[list(Test.columns)[-1]]

Oversampling = SMOTE(sampling_strategy='minority',random_state=seed)
X_train, y_train = Oversampling.fit_sample(X_train, y_train)

print('Total # of samples',len(y_train))
print('Total pos samples',sum(y_train))
print('Total neg samples',len(y_train)-sum(y_train))
print('Dimension of training data after over sampling',X_train.shape)

clf = SVC(kernel='sigmoid',random_state=seed)

# Train the model using the training sets
clf.fit(X_train,y_train)

# Training Stats
print('##############Training Stats##############')
# Predicting the values
y_pred=clf.predict(X_train)
# print(y_pred)
y_pred_cna_by_arm_training = y_pred
print_stats(y_train,y_pred)

print('##############Testing Stats##############')

# Predicting the values
y_pred=clf.predict(X_test)
y_pred_cna_by_arm = y_pred
# print(y_pred)

print_stats(y_test,y_pred)

#######################################################

print('#######################################################')

##################################
# Reading mutation_count_arm
##################################
print('##################################')
print('# Reading mutation_count_arm Data')
print('##################################')

data_cna_by_arm = pandas.read_csv('mutation_count_by_arm_+Pid.csv',sep='\t')
interm = data_cna_by_arm[list(data_cna_by_arm.columns)[2:]]

interm = Correlation_clean(interm,seed=seed,correlation_threshold=0.75,filename='corr_all_mutation_count_arm.png')
interm = Multivariate_analysis_MCA(interm,seed=seed)
interm_list = list(interm.columns)
interm_list.append('Patient')
data_cna_by_arm = data_cna_by_arm[interm_list]
Correlation_clean(data_cna_by_arm,seed=seed,correlation_threshold=0.5,filename='corr_selected_mutation_count_arm.png')
data_cna_by_arm = data_cna_by_arm.set_index('Patient')

Train = data_cna_by_arm.loc[X_train_list,:].reset_index()
Train = Train[list(Train.columns)[1:]]
Test = data_cna_by_arm.loc[X_test_list,:].reset_index()
Test = Test[list(Test.columns)[1:]]

print('---------------------------------------------------')
print('---------------------------------------------------')
print('interm list')
feature_names = pandas.DataFrame(columns=list(Train.columns)[:-1], dtype='object')
feature_names.to_csv('features_mutation_count_by_arm_+Pid.csv',sep='\t',index=False)
print(list(Train.columns))

print('---------------------------------------------------')
print('---------------------------------------------------')


X_train = Train[list(Train.columns)[:-1]]
y_train = Train[list(Train.columns)[-1]]

X_test = Test[list(Test.columns)[:-1]]
y_test = Test[list(Test.columns)[-1]]

Oversampling = SMOTE(sampling_strategy='minority',random_state=seed)
X_train, y_train = Oversampling.fit_sample(X_train, y_train)

print('Total # of samples',len(y_train))
print('Total pos samples',sum(y_train))
print('Total neg samples',len(y_train)-sum(y_train))
print('Dimension of training data after over sampling',X_train.shape)

clf = SVC(kernel='sigmoid',random_state=seed)

# Train the model using the training sets
clf.fit(X_train,y_train)

# Training Stats
print('##############Training Stats##############')
# Predicting the values
y_pred=clf.predict(X_train)
# print(y_pred)
y_pred_mutation_count_arm_training = y_pred

print_stats(y_train,y_pred)


print('##############Testing Stats##############')

# Predicting the values
y_pred=clf.predict(X_test)
y_pred_mutation_count_arm = y_pred
# print(y_pred)

print_stats(y_test,y_pred)

#######################################################

print('#######################################################')

##################################
# Reading mutation_matrix
##################################
print('##################################')
print('# Reading mutation_matrix Data')
print('##################################')


data_mutation_matrix = pandas.read_csv('mutation_matrix+Pid.csv',sep='\t')
interm = data_mutation_matrix[list(data_mutation_matrix.columns)[1:]]

interm = Correlation_clean(interm,seed=seed,correlation_threshold=0.5,filename='corr_all_mutation_matrix.png')
interm_list = list(interm.columns)

data_mutation_matrix = data_mutation_matrix[interm_list]
Correlation_clean(data_mutation_matrix,seed=seed,correlation_threshold=0.5,filename='corr_selected_mutation_matrix.png')
print(data_mutation_matrix)
data_mutation_matrix = data_mutation_matrix.set_index('Patient')
print(data_mutation_matrix)

Train = data_mutation_matrix.loc[X_train_list,:].reset_index()
Train = Train[list(Train.columns)[1:]]
Test = data_mutation_matrix.loc[X_test_list,:].reset_index()
Test = Test[list(Test.columns)[1:]]

print('---------------------------------------------------')
print('---------------------------------------------------')
print('interm list')
feature_names = pandas.DataFrame(columns=list(Train.columns)[:-1], dtype='object')
feature_names.to_csv('features_mutation_matrix+Pid.csv',sep='\t',index=False)
print(list(Train.columns))

print('---------------------------------------------------')
print('---------------------------------------------------')


X_train = Train[list(Train.columns)[:-1]]
y_train = Train[list(Train.columns)[-1]]

X_test = Test[list(Test.columns)[:-1]]
y_test = Test[list(Test.columns)[-1]]

Oversampling = SMOTE(sampling_strategy='minority',random_state=seed)
X_train, y_train = Oversampling.fit_sample(X_train, y_train)

print('Total # of samples',len(y_train))
print('Total pos samples',sum(y_train))
print('Total neg samples',len(y_train)-sum(y_train))
print('Dimension of training data after over sampling',X_train.shape)

#Create a Gaussian Classifier
clf=RandomForestClassifier(n_estimators=400,max_depth=3,n_jobs=4,random_state=seed)

# Train the model using the training sets
clf.fit(X_train,y_train)


# Training Stats
print('##############Training Stats##############')
# Predicting the values
y_pred=clf.predict(X_train)
# print(y_pred)
y_pred_mutation_matrix_training = y_pred

print_stats(y_train,y_pred)

print('##############Testing Stats##############')


# Predicting the values
y_pred=clf.predict(X_test)
y_pred_mutation_matrix = y_pred
# print(y_pred)

print_stats(y_test,y_pred)

#######################################################

print('#######################################################')

##################################
# Reading cna_by_gene_reduced
##################################

print('##################################')
print('# Reading cna_by_gene_reduced Data')
print('##################################')


data_mutation_matrix = pandas.read_csv('cna_by_gene_reduced+Pid.csv',sep='\t')
# interm = data_mutation_matrix[list(data_mutation_matrix.columns)[1:]]
interm = data_mutation_matrix  

interm = Correlation_clean(interm,seed=seed,correlation_threshold=0.5,filename='corr_all_cna_by_gene_reduced.png')
interm_list = list(interm.columns)

data_mutation_matrix = data_mutation_matrix[interm_list]
Correlation_clean(data_mutation_matrix,seed=seed,correlation_threshold=0.5,filename='corr_selected_cna_by_gene_reduced.png')
print(data_mutation_matrix)
data_mutation_matrix = data_mutation_matrix.set_index('Patient')
print(data_mutation_matrix)

Train = data_mutation_matrix.loc[X_train_list,:].reset_index()
Train = Train[list(Train.columns)[1:]]
Test = data_mutation_matrix.loc[X_test_list,:].reset_index()
Test = Test[list(Test.columns)[1:]]

print('---------------------------------------------------')
print('---------------------------------------------------')
print('interm list')
feature_names = pandas.DataFrame(columns=list(Train.columns)[:-1], dtype='object')
feature_names.to_csv('features_cna_by_gene_reduced+Pid.csv',sep='\t',index=False)
print(list(Train.columns))

print('---------------------------------------------------')
print('---------------------------------------------------')


X_train = Train[list(Train.columns)[:-1]]
y_train = Train[list(Train.columns)[-1]]

X_test = Test[list(Test.columns)[:-1]]
y_test = Test[list(Test.columns)[-1]]

Oversampling = SMOTE(sampling_strategy='minority',random_state=seed)
X_train, y_train = Oversampling.fit_sample(X_train, y_train)

print('Total # of samples',len(y_train))
print('Total pos samples',sum(y_train))
print('Total neg samples',len(y_train)-sum(y_train))
print('Dimension of training data after over sampling',X_train.shape)

#Create a Gaussian Classifier
clf=RandomForestClassifier(n_estimators=250,max_depth=3,n_jobs=4,random_state=seed)

# Train the model using the training sets
clf.fit(X_train,y_train)

# Training Stats
print('##############Training Stats##############')
# Predicting the values
# y_pred=clf.predict(X_train)
y_pred = (clf.predict_proba(X_train)[:,1] >= 0.31).astype(bool) # set threshold as 0.3
# print(y_pred)
y_pred_cna_by_gene_reduced_training = y_pred

print_stats(y_train,y_pred)

print('##############Testing Stats##############')

# Predicting the values

# y_pred=clf.predict(X_test)
y_pred = (clf.predict_proba(X_test)[:,1] >= 0.31).astype(bool) # set threshold as 0.3
# print(y_pred)
y_pred_gene_reduced = y_pred

print_stats(y_test,y_pred)

#######################################################
print('#######################################################')

##################################
# Reading gep_normad
##################################

print('##################################')
print('# Reading gep_normad Data')
print('##################################')


data_mutation_matrix = pandas.read_csv('gep_normed+Pid.csv',sep='\t')
interm = data_mutation_matrix[list(data_mutation_matrix.columns)[1:]]
interm = data_mutation_matrix  

interm = Correlation_clean(interm,seed=seed,correlation_threshold=0.5,filename='corr_all_gep_normad.png')
interm_list = list(interm.columns)

data_mutation_matrix = data_mutation_matrix[interm_list]
Correlation_clean(data_mutation_matrix,seed=seed,correlation_threshold=0.5,filename='corr_selected_gep_normad.png')
print(data_mutation_matrix)
data_mutation_matrix = data_mutation_matrix.set_index('Patient')
print(data_mutation_matrix)


Train = data_mutation_matrix.loc[X_train_list,:].reset_index()
Train = Train[list(Train.columns)[1:]]
Test = data_mutation_matrix.loc[X_test_list,:].reset_index()
Test = Test[list(Test.columns)[1:]]

print('---------------------------------------------------')
print('---------------------------------------------------')
print('interm list')
feature_names = pandas.DataFrame(columns=list(Train.columns)[:-1], dtype='object')
feature_names.to_csv('features_gep_normed+Pid.csv',sep='\t',index=False)
print(list(Train.columns))

print('---------------------------------------------------')
print('---------------------------------------------------')



X_train = Train[list(Train.columns)[:-1]]
y_train = Train[list(Train.columns)[-1]]

X_test = Test[list(Test.columns)[:-1]]
y_test = Test[list(Test.columns)[-1]]

Oversampling = SMOTE(sampling_strategy='minority',random_state=seed)
X_train, y_train = Oversampling.fit_sample(X_train, y_train)

print('Total # of samples',len(y_train))
print('Total pos samples',sum(y_train))
print('Total neg samples',len(y_train)-sum(y_train))
print('Dimension of training data after over sampling',X_train.shape)

#Create a Gaussian Classifier
clf=RandomForestClassifier(n_estimators=250,max_depth=3,n_jobs=4,random_state=seed)

# Train the model using the training sets
clf.fit(X_train,y_train)

# Training Stats
print('##############Training Stats##############')
# Predicting the values
# y_pred=clf.predict(X_train)
y_pred = (clf.predict_proba(X_train)[:,1] >= 0.22).astype(bool) # set threshold as 0.3
# print(y_pred)
y_pred_gep_normad_training = y_pred

print_stats(y_train,y_pred)

print('##############Testing Stats##############')

# Predicting the values

# y_pred=clf.predict(X_test)
y_pred = (clf.predict_proba(X_test)[:,1] >= 0.22).astype(bool) # set threshold as 0.3
# print(y_pred)
y_pred_gep_normed = y_pred

print_stats(y_test,y_pred)

#######################################################

##################################
# Reading clin_param Data
##################################
print('##################################')
print('# Reading clin_param Data')
print('##################################')

data_clin_param = pandas.read_csv('clin_param+Pid.csv',sep='\t')
data_clin_param = data_clin_param[ ['Patient','age',  'gender',  'iss',  'creatinine' ,  'risk']]
interm = data_clin_param[list(data_clin_param.columns)[1:]]

interm = Correlation_clean(interm,seed=seed,correlation_threshold=0.5,filename='corr_all_clin_param.png')
# interm = Multivariate_analysis(interm,seed=seed)
interm_list = list(interm.columns)
interm_list.append('Patient')
data_clin_param = data_clin_param[interm_list]
Correlation_clean(data_clin_param,seed=seed,correlation_threshold=0.5,filename='corr_selected_clin_param.png')
data_clin_param = data_clin_param.set_index('Patient')


Train = data_clin_param.loc[X_train_list,:].reset_index()
Train = Train[list(Train.columns)[1:]]
Test = data_clin_param.loc[X_test_list,:].reset_index()
Test = Test[list(Test.columns)[1:]]


print('---------------------------------------------------')
print('---------------------------------------------------')
print('interm list')
feature_names = pandas.DataFrame(columns=list(Train.columns)[:-1], dtype='object')
feature_names.to_csv('features_clin_param+Pid.csv',sep='\t',index=False)
print(list(Train.columns))

print('---------------------------------------------------')
print('---------------------------------------------------')



X_train = Train[list(Train.columns)[:-1]]
y_train = Train[list(Train.columns)[-1]]

X_test = Test[list(Test.columns)[:-1]]
y_test = Test[list(Test.columns)[-1]]

Oversampling = SMOTE(sampling_strategy='minority',random_state=seed)
X_train, y_train = Oversampling.fit_sample(X_train, y_train)

print('Total # of samples',len(y_train))
print('Total pos samples',sum(y_train))
print('Total neg samples',len(y_train)-sum(y_train))
print('Dimension of training data after over sampling',X_train.shape)

clf = SVC(kernel='sigmoid',random_state=seed)

# Train the model using the training sets
clf.fit(X_train,y_train)

# Training Stats
print('##############Training Stats##############')
# Predicting the values
y_pred=clf.predict(X_train)
# print(y_pred)
y_pred_clin_param_training = y_pred
# print_stats(y_train,y_pred)

print('##############Testing Stats##############')

# Predicting the values
y_pred=clf.predict(X_test)
y_pred_clin_param = y_pred
# print(y_pred)

print_stats(y_test,y_pred)

#######################################################

print('#######################################################')

print('#######################################################')
print('Saving Files')

prediction = pandas.DataFrame(dtype='object')
prediction['cna_by_arm'] = y_pred_cna_by_arm
prediction['mutation_count_arm'] = y_pred_mutation_count_arm
prediction['mutation_matrix'] = y_pred_mutation_matrix
prediction['cna_by_gene_reduced'] = y_pred_gene_reduced * 1
prediction['gep_normed'] = y_pred_gep_normed * 1
prediction['clin_param'] = y_pred_clin_param * 1
prediction['TrueClass'] = y_test

prediction.to_csv('score.csv',sep='\t',index=False)

training = pandas.DataFrame(dtype='object')
training['cna_by_arm'] = y_pred_cna_by_arm_training
training['mutation_count_arm'] = y_pred_mutation_count_arm_training
training['mutation_matrix'] = y_pred_mutation_matrix_training
training['cna_by_gene_reduced'] = y_pred_cna_by_gene_reduced_training * 1
training['gep_normed'] = y_pred_gep_normad_training * 1
training['clin_param'] = y_pred_clin_param_training * 1
training['TrueClass'] = y_train

training.to_csv('training_prediction.csv',sep='\t',index=False)
