import pandas, numpy

from sklearn import metrics
from sklearn.metrics import roc_curve

import matplotlib.pyplot as plt


def Correlation_clean(data,seed=10,correlation_threshold=0.75,filename='Correlation_Confusion_Matrix.png'):
    """
    docstring
    """
    seed = seed
    # ###########################################################################
    # Feature selection based on correlation
    # ################################################################
    corr = data.corr().abs()
    fig = plt.figure(figsize=(80, 80), dpi=100)
    ax = fig.add_subplot(111)
    cax = ax.matshow(corr,cmap='coolwarm', vmin=0, vmax=1)
    fig.colorbar(cax)
    ticks = numpy.arange(0,len(data.columns),1)
    ax.set_xticks(ticks)
    plt.xticks(rotation=90)
    ax.set_yticks(ticks)
    ax.set_xticklabels(data.columns)
    ax.set_yticklabels(data.columns)
    # plt.show()

    plt.savefig(filename)

    # Select upper triangle of correlation matrix
    upper = corr.where(numpy.triu(numpy.ones(corr.shape), k=1).astype(numpy.bool))
    # print(upper)
    # Find index of feature columns with correlation greater than 0.95
    to_drop = [column for column in upper.columns if any(upper[column] > correlation_threshold)]

    print('Columns with high corellation')
    print(to_drop)

    # Drop features 
    data = data.drop(data[to_drop], axis=1)
    print(data)

    return data

def Multivariate_analysis(data,seed=10):
    #################
    ## # Multivariate Analysis
    #################
    seed = 10
    import statsmodels.api as sm

    feature_matrix = data[list(data.columns)[:-1]]
    target = data[list(data.columns)[-1]]

    gamma_model = sm.OLS(target, sm.add_constant(feature_matrix))
    gamma_results = gamma_model.fit()

    print(gamma_results.summary())

    pval = list(gamma_results.pvalues)
    # print(pval)
    # print(dataset_col)
    pval_var = list(feature_matrix.columns)

    print(len(pval), len(pval_var))

    pcut = 0.05
    pvalueAnalysis = pandas.DataFrame(dtype='object')
    pvalueAnalysis['Variable'] = pval_var
    pvalueAnalysis['pvalues'] = pval
    sig = pvalueAnalysis[pvalueAnalysis['pvalues']<pcut]

    sig_var = list(sig['Variable'])
    sig_var.append('risk')
    print(sig_var)

    data = data[sig_var]
    print(data)

    return data

def Multivariate_analysis_MCA(data,seed=10):
    #################
    ## # Multivariate Analysis
    #################
    seed = seed
    
    def chi2test(X,y,pcut=0.05,summary_table=True,oversample=True,random_states=30,return_sig_varlist=True):
        import random
        from imblearn.over_sampling import SMOTE
        from sklearn.feature_selection import chi2
        import pandas

        X_chi = X.copy()
        y_chi = y.copy()

        random.seed(random_states)

        if  oversample == True:
            Oversampling = SMOTE(sampling_strategy='minority',random_state=random_states,k_neighbors=2)
            X_chi, y_chi = Oversampling.fit_sample(X_chi, y_chi)

        feature_cols = list(X_chi.columns)

        chi_scores, pval = chi2(X_chi,y_chi)
        chi = pandas.DataFrame(dtype='object')
        chi['features'] = feature_cols
        chi['Score'] = chi_scores
        chi['pval'] = pval

        sig = chi[chi['pval']<pcut]
        if summary_table == True:
            print('\n------------------------------------------------------------------------')
            print('Significant Varaiables based on Chi-square Test and p-value cutoff < ',pcut)
            print('------------------------------------------------------------------------')
            print(sig)
            print('------------------------------------------------------------------------')

        
        if return_sig_varlist == True:
            return list(sig['features'])


    feature_matrix = data[list(data.columns)[:-1]]
    target = data[list(data.columns)[-1]]

    sig_var = chi2test(feature_matrix,target,random_states=seed)
    sig_var.append('risk')
    print(sig_var)

    data = data[sig_var]
    print(data)

    return data


def print_stats(y_test,y_pred):
    from sklearn import metrics
    from sklearn.metrics import roc_curve
    
    tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()
    print('Confusion Matrix\n',metrics.confusion_matrix(y_test,y_pred))
    print()
    print('Total positive test samples',tp+fn)
    print('Total negative test samples',fp+tn)
    print()
    print()
    print('False Negative', round(fn,4))
    print('False Positive', round(fp,4))
    print('True Negative', round(tn,4))
    print('True Positive', round(tp,4))
    print()
    print('Classification Report\n',metrics.classification_report(y_test,y_pred))
    print()
    print('False Negative rate', round(fn/(tp+fn),4))    
    print('False Positive rate', round(fp/(fp+tn),4))
    print()
    print('Specificity :',round(tn / (tn+fp),4))
    print('Sensitivity :', round(tp / (tp + fn),4))
    print()
    fpr, tpr, thresh = roc_curve(y_test, y_pred) 
    print('AUC - ROC :', round(metrics.auc(fpr, tpr),4))
    print("Accuracy:",round(metrics.accuracy_score(y_test, y_pred),4))
    print()
    print('F1 :', round(metrics.f1_score(y_test, y_pred),4))
    print('MCC:',round(metrics.matthews_corrcoef(y_test, y_pred),4))
    print()
    print('Mean Absolute Error:', round(metrics.mean_absolute_error(y_test, y_pred),4))
    print('Mean Squared Error:', round(metrics.mean_squared_error(y_test, y_pred),4))
    print('Root Mean Squared Error:', round(numpy.sqrt(metrics.mean_squared_error(y_test, y_pred)),4))
    print()


    from sklearn import metrics
    from sklearn.metrics import roc_curve
    
    tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()

    # ##False Negative rate
    fnR = round(fn/(tp+fn),4)
    # ##False Positive rate
    fpR = round(fp/(fp+tn),4)
    # ##Specificity
    spec = round(tn / (tn+fp),4)
    # ##Sensitivity
    sen = round(tp / (tp + fn),4)
    
    fpr, tpr, thresh = roc_curve(y_test, y_pred) 
    # ##ROC
    roc = round(metrics.auc(fpr, tpr),4)
    # ## Accuracy
    auc = round(metrics.accuracy_score(y_test, y_pred),4)
    # ##F1
    f1 = round(metrics.f1_score(y_test, y_pred),4)
    
    return [fnR, fpR, roc, auc, f1, sen, spec]



    from progress.bar import Bar
    from copy import deepcopy

    X_train_org  = X_train.copy()
    y_train_org  = y_train.copy()
    X_train_col = list(X_train.columns)
    
    # Storing Stats
    fnR = []
    fpR = []
    roc = []
    auc = []
    f1 = []
    sen = []
    spec = []

    y_true = []
    y_hat = []
    
    # progress bar
    bar = Bar('Processing sample #',max=len(X_train)+1)
    
    for n in range(len(X_train)):
        
        clf = deepcopy(initial_model)
        
        X_train = X_train_org.copy()
        X_train = X_train.drop([n]).reset_index()
        X_train = X_train[X_train_col]

        X_test = X_train_org.iloc[[n]]
        
        y_train = y_train_org.copy()
        y_train = y_train.drop([n]).reset_index()
        y_train = y_train['risk']
        
        y_test = y_train_org.iloc[[n]].values
            
        # Train the model using the training sets        
        clf.fit(X_train,y_train)
        
        # Predicting the values
        y_pred = (clf.predict_proba(X_test)[:,1] >= cutoff).astype(bool) # set threshold as 0.3
        
        y_true.append(y_test[0])
        y_hat.append(y_pred[0])

        bar.next()

    bar.finish()

    interm = pandas.DataFrame(dtype='object')
    interm['true'] = y_true
    interm['pred'] = y_hat

    # print("Accuracy:",round(metrics.accuracy_score(y_train, y_pred),4))
    auc = (round(metrics.accuracy_score(interm['true'], interm['pred']),4))
    # print('F1 :', round(metrics.f1_score(y_train, y_pred),4))
    f1 = (round(metrics.f1_score(interm['true'], interm['pred']),4))
    
    fpr, tpr, thresh = roc_curve(interm['true'], interm['pred']) 
    # print('AUC - ROC :', round(metrics.auc(fpr, tpr),4))
    roc = (round(metrics.auc(fpr, tpr),4))

    tn, fp, fn, tp = metrics.confusion_matrix(interm['true'], interm['pred']).ravel()
    # print('Specificity :',round(tn / (tn+fp),4))
    sen = (round(tn / (tn+fp),4))
    # print('Sensitivity :', round(tp / (tp + fn),4))
    spec = (round(tp / (tp + fn),4))
        
    # print('False Negative rate', round(fn/(tp+fn),4))
    fnR = (round(fn/(tp+fn),4))
    # print('False Positive rate', round(fp/(fp+tn),4))
    fpR = (round(fp/(fp+tn),4))

    stats  = pandas.DataFrame(dtype='object')
    
    stats.at[0,'fnr'] = fnR
    stats.at[0,'fpr'] = fpR
    stats.at[0,'roc'] = roc
    stats.at[0,'auc'] = auc
    stats.at[0,'f1'] = f1
    stats.at[0,'sen'] = sen
    stats.at[0,'spec']= spec

    # print(stats)

    return stats
